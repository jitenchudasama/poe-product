
/*=======================================================
Created by : Ritesh
Created Date : 14th Dec 2011
Description :	Create Schedules From Call Maintenance
Modified By : Sanjay Patel
Date		: 17-MAR-2015
Desc		: Sent Mobile Generate data back to update notes in HHACG for GPS data.  
========================================================
--  Modified By     : Mayur Borad
--  Modified Date   : 15 APR 2015
--  Description	    : Automatic Creation of Schedules Status - retuen validation message details
--  Version			: ENT 6.0.2.9
========================================================
--  Modified By     : Shivam Parikh
--  Modified Date   : 06/18/2015
--  Description	    : Auto create schedule - Added Space between Patient Name and Patient Number, return IsSkilled Visit
--  Version			: ENT 6.0.3.1
========================================================
--  Modified By     : Darshan Shah
--  Modified Date   : 06/26/2015
--  Description	    : fixed bug of not wrong alias of IsSkilledschedule
--  Version			: ENT 6.0.3.1
========================================================
--  Modified By     : Mayur Borad
--  Modified Date   : 30 JUN 2015
--  Description	    : Automatic Creation of Schedules Status - Get AdmissionID and Aidecode with Office Code
--  Version			: ENT 6.0.2.9
========================================================
--  Modified By     : Shivam Parikh
--  Modified Date   : 07/06/2015
--  Description	    : OT and Compliance validations
--  Version			: ENT 6.0.2.9
--  Task ID			: 100644
========================================================
--  Modified By     : Shivam Parikh
--  Modified Date   : 08/06/2015
--  Description	    : Add AgencyID parameter
--  Task ID			: 103297
========================================================
--  Modified By     : Shivam Parikh
--  Modified Date   : 08/18/2015
--  Description	    : Added a new parameter "SkipSuccessFailureSelection"
--						If the value of this parameter is set to 1 - it 
--						will not get list of success and failure visits
--  Task ID			: 103297
========================================================
--  Modified By     : Apoorva
--  Modified Date   : 15 JUN 2016
--  Description	    : Added for Linked Contracts
========================================================
-- Modified By		: Kalyani Pallagani
-- Modified Date	: 12-Sep-2016
-- Remarks			: Live Issue  : Code changed to get distinct Failure and Success Visits
-- =========================================================================================================
-- Modified By		: Kalyani Pallagani
-- Modified Date	: 14-Sep-2016
-- Remarks			: Non-Skill visit is created instead of Skill visit
-- =========================================================================================================
-- Task ID     : HHAXENT-1913
-- Added By    : Indravijay
-- Added Date  : 8-DEC-2016
-- Purpose     : Configuration to allow Visit/Absence Overlaps
-- ===================================================================================
-- Added By    : Maulik Suthar
-- Added Date  : 08 August 2018
-- Task ID     : ENTML-20034 Re-Write Pre-Billing Process and Page for enterprise providers, Return VisitId/LastModifiedDate As XML
-- Version	   : ENT-12.0.1.0 
-- ====================================================================================
-- Added By    : Maulik Suthar
-- Added Date  : 25 Sept 2018
-- Task ID     : DB Review
-- Version	   : ENT-13.0.1.0 
-- =========================================================================================================
-- Modified By		: Dharmesh Patil
-- Modified Date	: 07-Feb-2019
-- Remarks			: ENTML-27471 ENT Validation: Automatic Creation of Schedule (Call Dashboard)
-- =========================================================================================================
-- Modified By		: Chandrakant
-- Modified Date	: 16-Nov-2019
-- Remarks			: Bug Fix CRM-2840 calling 18.6 version sp 
-- =========================================================================================================

DECLARE @CallsXML XML
SELECT	@CallsXML = 
'<Visits>
	<Data>
		<Visit Date="05/25/2011" SS="0355" SE="0700" AideID="60035" PatientID="23382" PayRateID="684" ContractID1="103" RateID1="222" Hours1="03" Minutes1="05" ContractID2="-1" RateID2="-1" POCHeaderID="-1" IsTempSchedule="1" IsTempAide="1" CallInMID="" CallOut
MID="94417"/>
	</Data>
	<Data>
		<Visit Date="06/03/2011" SS="0752" SE="0800" AideID="66032" PatientID="23382" PayRateID="1698" ContractID1="103" RateID1="3680" Hours1="00" Minutes1="08" ContractID2="-1" RateID2="-1" POCHeaderID="-1" IsTempSchedule="1" IsTempAide="1" CallInMID="" CallO
utMID="101988"/>
	</Data>
	<Data>
		<Visit Date="06/01/2011" SS="0630" SE="0900" AideID="72144" PatientID="23382" PayRateID="12" ContractID1="103" RateID1="3319" Hours1="02" Minutes1="30" ContractID2="-1" RateID2="-1" POCHeaderID="-1" IsTempSchedule="1" IsTempAide="1" CallInMID="" CallOut
MID="99972" />
	</Data>
</Visits>'

--select @CallsXML

EXEC [dbo].[CAL_CreateSchedulesFromCallMaintenance] @UserID = 106,@CallsXML = @CallsXML
,@AppVersion='ENT',@Version=5.5,@IsSaved=0

DECLARE @IsSaved INT = 0
EXEC [dbo].[CAL_CreateSchedulesFromCallMaintenance]
@UserID = 106,
@CallsXML = '
<Visits>
	<Data>
		<Visit Date=''2015-04-28 08:49:56.363''
		SS=''849''
		SE=''852''
		AideID=''422591''
		PatientID=''921377''
		PayRateID=''-1''
	   ContractID1=''97''
		RateID1=''-1''
		Hours1=''0''
		Minutes1=''3''
		POCHeaderID=''-1''
		IsTempSchedule=''0''
		IsTempAide=''0''
		CallInMID=''11775''
		CallOutMID=''11849''
		IsSkilledSchedule=''-1'' />
	</Data>
</Visits>
',
@UpdateWithNoAuthorization = 1,
@UpdateScheduleOverTime = 0,
@UpdateWithAideCompliant = 0,
@IsSaved = @IsSaved OUTPUT,
@AppVersion	= 'ENT',
@Version = 6.5,
@MinorVersion = 5.3,
@CallerInfo	= '',
@IncludeMultipleVisits  = 0,
@OverrideReasonID = -1

SELECT @IsSaved
EXEC [dbo].[CAL_CreateSchedulesFromCallMaintenance] @UserID = 106,@CallsXML = '<Visits><Data><Visit Date="06/01/2016" SS="0515" SE="0615" AideID="475801" PatientID="732864" PayRateID="9109" ContractID1="4" RateID1="35488" Hours1="01" Minutes1="00" POCHead
erID="-1" IsTempSchedule="1" IsTempAide="1" CallInMID="" CallOutMID="1101" IsSkilledSchedule="2"/></Data></Visits>'
,@AppVersion='ENT',@Version=6.6,@MinorVersion = 3.7,@CallerInfo='System.RuntimeMethodHandle.InvokeMethod->Call_AutoScheduleCreationFromCallMaintenance.CreateSchedulesFromCallMaintenance->HHAExchangeDL.EIN.HHAExchange.DL.CallOperations.CreateSchedulesFromC
allMaintenance'
,@UpdateWithNoAuthorization=0,@UpdateScheduleOverTime=1,@UpdateWithAideCompliant=0,@OverrideReasonID=10,@IncludeMultipleVisits=1,@IsSaved=0


*/

CREATE PROCEDURE [dbo].[CAL_CreateSchedulesFromCallMaintenance]
(
	@UserID							INT,
	@CallsXML						XML,	
	@UpdateWithNoAuthorization		BIT = 0,
	@UpdateScheduleOverTime			BIT = 0,
	@UpdateWithAideCompliant		BIT = 0,
	@IsSaved						INT = 0 OUT,
	@AppVersion						VARCHAR(10)		= 'ENT' ,
	@Version						NUMERIC(4,2)	= 0.0 ,
	@MinorVersion					NUMERIC(4,2)	= 0.0,
	@CallerInfo						VARCHAR(1024)   = '',
	@IncludeMultipleVisits			BIT = 0,
	@OverrideReasonID				INT = -1,	-- Added by Shivam Parikh - 07/07/2015 Task : 100644
	@AutoProcessUserID				INT = NULL,-- Added By Shivam Parikh - Task : 103297
	@SkipSuccessFailureSelection	BIT = 0, -- Added by Shivam Parikh - 08/18/2015
	@ValidateCaregiverAbsentOverlap	BIT = 1,
	@SkipAbsenceWarning			    BIT = 0,
	@VisitChangeData				XML = NULL OUT
)
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON
		SET XACT_ABORT ON;

		DECLARE @AppVer INT
		SELECT @AppVer = VersionInt FROM dbo.GetVersionInt(@Version, @MinorVersion)

		DECLARE @ENT17000100 INT
		SELECT @ENT17000100 = VersionInt FROM dbo.GetVersionInt(17.0, 1.0)
		
		DECLARE @ENT18060100 INT
		SELECT @ENT18060100 = VersionInt FROM dbo.GetVersionInt(18.6, 1.0)
		IF @AppVersion = 'ENT' AND @AppVer >= @ENT18060100 BEGIN
		   EXEC [dbo].[CAL_CreateSchedulesFromCallMaintenance_ENT18060100]   
			     @UserID							= @UserID
				,@CallsXML							= @CallsXML
				,@UpdateWithNoAuthorization			= @UpdateWithNoAuthorization
				,@UpdateScheduleOverTime			= @UpdateScheduleOverTime
				,@UpdateWithAideCompliant			= @UpdateWithAideCompliant
				,@IsSaved							= @IsSaved OUT
				,@AppVersion						= @AppVersion
				,@Version							= @Version
				,@MinorVersion						= @MinorVersion
				,@CallerInfo						= @CallerInfo
				,@IncludeMultipleVisits				= @IncludeMultipleVisits
				,@OverrideReasonID					= @OverrideReasonID
				,@AutoProcessUserID					= @AutoProcessUserID
				,@SkipSuccessFailureSelection		= @SkipSuccessFailureSelection
				,@ValidateCaregiverAbsentOverlap	= @ValidateCaregiverAbsentOverlap
				,@SkipAbsenceWarning				= @SkipAbsenceWarning
				,@VisitChangeData					= @VisitChangeData OUT
			RETURN			       
		END

		IF @AppVersion = 'ENT' AND @AppVer >= @ENT17000100 BEGIN
		   EXEC [dbo].[CAL_CreateSchedulesFromCallMaintenance_ENT17000100]   
			     @UserID							= @UserID
				,@CallsXML							= @CallsXML
				,@UpdateWithNoAuthorization			= @UpdateWithNoAuthorization
				,@UpdateScheduleOverTime			= @UpdateScheduleOverTime
				,@UpdateWithAideCompliant			= @UpdateWithAideCompliant
				,@IsSaved							= @IsSaved OUT
				,@AppVersion						= @AppVersion
				,@Version							= @Version
				,@MinorVersion						= @MinorVersion
				,@CallerInfo						= @CallerInfo
				,@IncludeMultipleVisits				= @IncludeMultipleVisits
				,@OverrideReasonID					= @OverrideReasonID
				,@AutoProcessUserID					= @AutoProcessUserID
				,@SkipSuccessFailureSelection		= @SkipSuccessFailureSelection
				,@ValidateCaregiverAbsentOverlap	= @ValidateCaregiverAbsentOverlap
				,@SkipAbsenceWarning				= @SkipAbsenceWarning
				,@VisitChangeData					= @VisitChangeData OUT
			RETURN			       
		END

		-- Added By Shivam Parikh - TAsk : 103297
		DECLARE @ActionByUserID INT
		SELECT @ActionByUserID = CASE WHEN @AutoProcessUserID < 0  THEN @AutoProcessUserID ELSE @UserID END

		DECLARE @ENT06000200 INT
		SELECT @ENT06000200 = VersionInt FROM dbo.GetVersionInt(6.0, 2.0)

		DECLARE @ENT06000209 INT
		SELECT @ENT06000209 = VersionInt FROM dbo.GetVersionInt(6.0, 2.9)

		DECLARE @ENT06050507 INT
		SELECT @ENT06050507 = VersionInt FROM dbo.GetVersionInt(6.5, 5.7)

		DECLARE @ENT06060403 INT
		SELECT @ENT06060403 = VersionInt FROM dbo.GetVersionInt(6.6, 4.3)

		--Added By: Maulik Suthar_ENTML-20034_08082018
		DECLARE @CompanyID INT
		DECLARE @xmlVisitChangeData AS VARCHAR(MAX)
		DECLARE @ENT13000100 INT
		SELECT  @ENT13000100 = VersionInt FROM dbo.GetVersionInt(13.0, 1.0)
		SET @xmlVisitChangeData = ''

		SELECT @CompanyID = ID 
		FROM dbo.TblUserMaster WITH (NOLOCK) 
		WHERE UserID = @UserID

		DECLARE @cnt INT, @inc INT, @VisitID BIGINT, @CallInMID BIGINT,@CallOutMID BIGINT, @DisplayMessage VARCHAR(MAX) 
		DECLARE @ScheduleInfo XML
		DECLARE @IsSkilledSchedule INT,
				@MaintenanceID INT = 0,
				@CallUniqueID VARCHAR(100),
				@PatientID INT
		SELECT @inc = 1	
        DECLARE @VisitInfo XML
		DECLARE @PayRateID INT = -1
		--===============================================================================================================
		-- Hash Table Declarations
		--===============================================================================================================
		IF OBJECT_ID('tempdb..#ReturnValues_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #ReturnValues_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#SuccessedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #SuccessedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#FailedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #FailedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#VisitInfoDetailUnique_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #VisitInfoDetailUnique_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		
		CREATE TABLE #ReturnValues_dbo_CAL_CreateSchedulesFromCallMaintenance
			(	
				AutoID INT		IDENTITY(1,1),
				VisitID			BIGINT, 
				CallUniqueID	VARCHAR(100)
			)

		CREATE TABLE #SuccessedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance
			(	
				AutoID INT		IDENTITY(1,1),
				VisitInfoAutoID	INT, 
				VisitID			BIGINT,
				CallUniqueID	VARCHAR(100)
			)

		CREATE TABLE #FailedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance
		(	
			AutoID INT		IDENTITY(1,1),
			VisitInfoAutoID	INT, 
			CallUniqueID	VARCHAR(100),
			Problem			VARCHAR(MAX),
			IsSaved			INT -- Added By Shivam Parikh - 07/08/2015 Task :100644
		)

		CREATE TABLE #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance 
		(	
			AutoID INT IDENTITY(1,1),
			VisitDate DATETIME,
			SS VARCHAR(4),
			SE VARCHAR(4),
			AideID INT,
			PatientID INT,
			PayRateID INT,
			ServiceCodeID INT,
			ContractID1 INT,
			ContractID2 INT,
			RateID1 INT,
			RateID2 INT,			
			POC INT,
			Hours1 INT,
			Minutes1 INT,
			Hours2 INT,
			Minutes2 INT,
			AuthorizationID	INT,
			IsTemporary BIT,
			IsTempAide BIT,
			IsDutyFree BIT,
			ImportReferenceNumber VARCHAR(50),
			CallInMID INT,
			CallOutMID INT,
  		    IsSkilledSchedule INT,
			ScheduleXML XML
		)

		CREATE TABLE #VisitInfoDetailUnique_dbo_CAL_CreateSchedulesFromCallMaintenance 
		(	
			AutoID INT IDENTITY(1,1),
			VisitDate DATETIME,
			SS VARCHAR(4),
			SE VARCHAR(4),
			AideID INT,
			PatientID INT,
			PayRateID INT,
			ServiceCodeID INT,
			ContractID1 INT,
			ContractID2 INT,
			RateID1 INT,
			RateID2 INT,			
			POC INT,
			Hours1 INT,
			Minutes1 INT,
			Hours2 INT,
			Minutes2 INT,
			AuthorizationID	INT,
			IsTemporary BIT,
			IsTempAide BIT,
			IsDutyFree BIT,
			ImportReferenceNumber VARCHAR(50),
			CallInMID INT,
			CallOutMID INT,
  		    IsSkilledSchedule INT,
			ScheduleXMLData VARCHAR(MAX)
		)
		--===============================================================================================================
		-- Added By : Mayur Borad - Automatic Creation of Schedules Status
		IF @AppVersion='ENT' AND  @AppVer  >= @ENT06000209 BEGIN	
			--===========================================================================================================
			-- Read Data from XML
			--===========================================================================================================
			INSERT INTO #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance
			(
				VisitDate,
				SS,
				SE,
		    	AideID,
				PatientID,
		    	PayRateID,
				ContractID1,
				ContractID2,
				RateID1,
				RateID2,			
		    	POC,
				Hours1,
				Minutes1,
				Hours2,
				Minutes2,
				AuthorizationID,
				IsTemporary,
				IsTempAide,
				IsDutyFree,
				ImportReferenceNumber,
				CallInMID,
				CallOutMID,
				IsSkilledSchedule,
				ScheduleXML
			)
			SELECT 
				d.v.value('@Date','DATETIME'),
				d.v.value('@SS','VARCHAR(4)'),
				d.v.value('@SE','VARCHAR(4)'),
				d.v.value('@AideID','INT'),
				d.v.value('@PatientID','INT'),
				d.v.value('@PayRateID','INT'),
				d.v.value('@ContractID1','INT'),
				d.v.value('@ContractID2','INT'),
				d.v.value('@RateID1','INT'),
				d.v.value('@RateID2','INT'),			
				NULLIF(d.v.value('@POC','INT'),-1),
				d.v.value('@Hours1','INT'),
				d.v.value('@Minutes1','INT'),
				d.v.value('@Hours2','INT'),
				d.v.value('@Minutes2','INT'),
				d.v.value('@AuthorizationID','INT'),
				d.v.value('@IsTemporary','BIT'),
				d.v.value('@IsTempAide','BIT'),
				d.v.value('@IsDutyFree','BIT'),
		    	d.v.value('@ImportReferenceNumber','VARCHAR(50)'),
				ISNULL(d.v.value('@CallInMID','INT'),0),
				ISNULL(d.v.value('@CallOutMID','INT'),0),
				d.v.value('@IsSkilledSchedule','INT'),
				d.v.query('.')
			FROM @CallsXML.nodes('/Visits/Data/Visit') d(v)		
			--===========================================================================================================
			--
			--Added Distinct code for ENT 6.6.4.3
			IF @AppVersion='ENT' AND @AppVer >= @ENT06060403
            BEGIN
				INSERT INTO #VisitInfoDetailUnique_dbo_CAL_CreateSchedulesFromCallMaintenance
				(
					VisitDate,
					SS,
					SE,
		    		AideID,
					PatientID,
		    		PayRateID,
					ContractID1,
					ContractID2,
					RateID1,
					RateID2,			
		    		POC,
					Hours1,
					Minutes1,
					Hours2,
					Minutes2,
					AuthorizationID,
					IsTemporary,
					IsTempAide,
					IsDutyFree,
					ImportReferenceNumber,
					CallInMID,
					CallOutMID,
					IsSkilledSchedule,
					ScheduleXMLData
				)
				SELECT DISTINCT
					VisitDate,
					SS,
					SE,
					AideID,
					PatientID,
					PayRateID,
					ContractID1,
					ContractID2,
					RateID1,
					RateID2,			
					POC,
					Hours1,
					Minutes1,
					Hours2,
					Minutes2,
					AuthorizationID,
					IsTemporary,
					IsTempAide,
					IsDutyFree,
					ImportReferenceNumber,
					CallInMID,
					CallOutMID,
					IsSkilledSchedule,
					CONVERT(VARCHAR(MAX), ScheduleXML) AS ScheduleXMLData
				FROM #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance
				
				TRUNCATE TABLE #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance

				INSERT INTO #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance
				(
					VisitDate,
					SS,
					SE,
					AideID,
					PatientID,
					PayRateID,
					ContractID1,
					ContractID2,
					RateID1,
					RateID2,			
					POC,
					Hours1,
					Minutes1,
					Hours2,
					Minutes2,
					AuthorizationID,
					IsTemporary,
					IsTempAide,
					IsDutyFree,
					ImportReferenceNumber,
					CallInMID,
					CallOutMID,
					IsSkilledSchedule,
					ScheduleXML
				)
				SELECT 
					VisitDate,
					SS,
					SE,
					AideID,
					PatientID,
					PayRateID,
					ContractID1,
					ContractID2,
					RateID1,
					RateID2,			
					POC,
					Hours1,
					Minutes1,
					Hours2,
					Minutes2,
					AuthorizationID,
					IsTemporary,
					IsTempAide,
					IsDutyFree,
					ImportReferenceNumber,
					CallInMID,
					CallOutMID,
					IsSkilledSchedule,
					CONVERT(XML,ScheduleXMLData, 1) AS ScheduleXML
				FROM #VisitInfoDetailUnique_dbo_CAL_CreateSchedulesFromCallMaintenance

				TRUNCATE TABLE #VisitInfoDetailUnique_dbo_CAL_CreateSchedulesFromCallMaintenance
			END

			SELECT @cnt = COUNT(1)
			FROM #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance
			--===========================================================================================================
			-- Transaction
			--===========================================================================================================	
			WHILE @inc <= @cnt BEGIN
				SELECT  @VisitID = NULL,
						@MaintenanceID = 0,
						@CallUniqueID = '0',
						@DisplayMessage = NULL,
                        @IsSkilledSchedule = 0,
                        @ScheduleInfo = NULL,
                        @VisitInfo='<Visit></Visit>',
						@PayRateID = -1

				SELECT	
					@IsSkilledSchedule	= CASE 
											    WHEN v.IsSkilledSchedule = 2 THEN 0 
											ELSE 1 
										END, -- 0 - Non Skilled Visit,  1 - Skilled visit, 2 - Non Skilled Visit
					@MaintenanceID		= CASE 
										        WHEN ISNULL(v.CallInMID,0) = 0 THEN v.CallOutMID
												ELSE ISNULL(v.CallInMID,0)
											END,
					@ScheduleInfo = v.ScheduleXML,
					@PatientID          =   v.PatientID,
					@PayRateID			=   v.PayRateID
				FROM  #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance v
				WHERE AutoID = @inc;

                SELECT @VisitInfo = '<Visit Reason="-1" 
                                            Notes="" 
                                            IsValidatePage="0" 
                                            TimesheetRequired="0" 
                                            TimesheetReceived="0" 
                                            IsTempSchedule="0" 
                                            IsTempAide="0" 
                                            CaregiverNote="" 
                                            VisitAuditVerifiedBy="0" 
                                            VisitAuditVerifiedByOther="" 
                                            VisitAuditDateTime="" 
                                            VisitAuditSupervisor="" 
                                            AbsenceCode="-1" 
                                            OverTimeCode="-1" 
                                            VNSFullServiceCode="-1" 
                                            PayRateID="' + CAST(@PayRateID AS VARCHAR) + '" 
                                            VisitEditActionTakenReasonID="-1">
                                     </Visit>'
				IF DATALENGTH(@ScheduleInfo) > 0 BEGIN						
                    BEGIN TRAN	
					---Modified by Apoorva for Linked Contracts---
					IF dbo.IsInternalPatient(@PatientID)=0 
					BEGIN
					    IF @IncludeMultipleVisits = 1 BEGIN -- If called from save multiple visits	
						    EXEC [Visit].[SaveScheduleInfoPayer]
							    @UserID						= @UserID,
							    @VisitID					= -1,
							    @ScheduleInfo				= @ScheduleInfo,
							    @IsSaved					= @IsSaved OUTPUT,
							    @AppVersion					= @AppVersion,
							    @Version					= @Version,
							    @MinorVersion				= @MinorVersion,
							    @DisplayMessage				= @DisplayMessage OUTPUT,
							    @IsSkilledVisit				= @IsSkilledSchedule,
							    @UpdateScheduleOverTime		= @UpdateScheduleOverTime,
							    @UpdateWithCaregiverCompliant	= @UpdateWithAideCompliant,	
							    @UpdateWithNoAuthorization	= @UpdateWithNoAuthorization,
							    @CallerInfo					= @CallerInfo,
							    @OverrideReasonID			= @OverrideReasonID, 
								@ValidateCaregiverAbsentOverlap = @ValidateCaregiverAbsentOverlap,
								@SkipAbsenceWarning		    = @SkipAbsenceWarning,
								@VisitChangeData			= @VisitChangeData OUT
					    END ELSE
                        BEGIN
						    IF @IsSkilledSchedule = 0 BEGIN -- 0 - Non Skilled Visit,  1 - Skilled visit
							    EXEC [Visit].[SaveScheduleInfoPayer]
							    @UserID						= @UserID,
							    @VisitID					= -1,
							    @ScheduleInfo				= @ScheduleInfo,
							    @IsSaved					= @IsSaved OUTPUT,
							    @AppVersion					= @AppVersion,
							    @Version					= @Version,
							    @MinorVersion				= @MinorVersion,
							    @DisplayMessage				= @DisplayMessage OUTPUT,
							    @IsSkilledVisit				= @IsSkilledSchedule,
							    @UpdateScheduleOverTime		= @UpdateScheduleOverTime,
							    @UpdateWithCaregiverCompliant	= @UpdateWithAideCompliant,	
							    @UpdateWithNoAuthorization	= @UpdateWithNoAuthorization,
							    @CallerInfo					= @CallerInfo,
							    @OverrideReasonID			= @OverrideReasonID,
								@ValidateCaregiverAbsentOverlap = @ValidateCaregiverAbsentOverlap,
								@SkipAbsenceWarning		    = @SkipAbsenceWarning,
								@VisitChangeData			= @VisitChangeData OUT
						    END ELSE 
						    BEGIN
							    EXEC [Visit].[SaveSkilledScheduledAndVisitInfoPayer]
								    @UserID						= @UserID,					
								    @VisitID					= -1,				
								    @ScheduleData				= @ScheduleInfo,				
                                    @VisitData					    = @VisitInfo,
								    @IsSaved					= @IsSaved OUT,
								    @UpdateScheduleOverTime		= @UpdateScheduleOverTime,	
								    @UpdateWithCaregiverCompliant	= @UpdateWithAideCompliant,
								    @UpdateWithNoAuthorization	= @UpdateWithNoAuthorization,
								    @AppVersion					= @AppVersion,				
								    @Version					= @Version,
								    @MinorVersion				= @MinorVersion,
								    @NotSavedMessage			= @DisplayMessage OUT,
									@IsSkilledVisit					= @IsSkilledSchedule,
								    @CallerInfo					= @CallerInfo,
								    @OverrideReasonID			= @OverrideReasonID,
									@ValidateCaregiverAbsentOverlap = @ValidateCaregiverAbsentOverlap,
									@SkipAbsenceWarning			= @SkipAbsenceWarning,
									@VisitChangeData			= @VisitChangeData OUT
						    END
					    END
					END
					ELSE IF ISNULL(@IncludeMultipleVisits, 0) = 1 
					BEGIN -- If called from save multiple visits	
						EXEC [Visit].[SaveScheduledInfoNew]
							@UserID						= @UserID,
							@VisitID					= -1,
							@Data						= @ScheduleInfo,
							@IsSaved					= @IsSaved OUTPUT,
							@AppVersion					= @AppVersion,
							@Version					= @Version,
							@MinorVersion				= @MinorVersion,
							@DisplayMessage				= @DisplayMessage OUTPUT,
							@IsSkilledVisit				= @IsSkilledSchedule,
							@UpdateScheduleOverTime		= @UpdateScheduleOverTime,
							@UpdateWithAideCompliant	= @UpdateWithAideCompliant,	
							@UpdateWithNoAuthorization	= @UpdateWithNoAuthorization,
							@CallFromParentSP			= 0,   -- is called from parent page									
							@CallerInfo					= @CallerInfo,
							@OverrideReasonID			= @OverrideReasonID, -- added BY Shivam Parikh - 07/07/2015 Task : 100644
							@ValidateCaregiverAbsentOverlap = @ValidateCaregiverAbsentOverlap,
							@SkipAbsenceWarning		    = @SkipAbsenceWarning,
							@VisitChangeData			= @VisitChangeData OUT
					END 
					ELSE BEGIN	
						IF @IsSkilledSchedule = 0 BEGIN -- 0 - Non Skilled Visit,  1 - Skilled visit
							EXEC [Visit].[SaveScheduledInfo]
								@UserID						= @UserID,
								@VisitID					= -1,
								@Data						= @ScheduleInfo,
								@IsSaved					= @IsSaved OUT,
								@UpdateScheduleOverTime		= @UpdateScheduleOverTime,
								@UpdateWithAideCompliant	= @UpdateWithAideCompliant,
								@UpdateWithNoAuthorization	= @UpdateWithNoAuthorization,
								@AppVersion					= @AppVersion,
								@Version					= @Version,
								@MinorVersion				= @MinorVersion,								
								@CallerInfo					= @CallerInfo,
								@OverrideReasonID			= @OverrideReasonID, -- added BY Shivam Parikh - 07/07/2015 Task : 100644
								@AutoProcessUserID			= @AutoProcessUserID, -- Adde By Shivam Parikh - Task : 103297
								@ValidateCaregiverAbsentOverlap = @ValidateCaregiverAbsentOverlap,
								@SkipAbsenceWarning		    = @SkipAbsenceWarning,
								@VisitChangeData			= @VisitChangeData OUT
						END ELSE BEGIN
							EXEC [Visit].[SaveSkilledScheduledInfo]
								@UserID						= @UserID,					
								@VisitID					= -1,				
								@Data						= @ScheduleInfo,				
								@IsSaved					= @IsSaved OUT,
								@UpdateScheduleOverTime		= @UpdateScheduleOverTime,	
								@UpdateWithAideCompliant	= @UpdateWithAideCompliant,
								@UpdateWithNoAuthorization	= @UpdateWithNoAuthorization,
								@AppVersion					= @AppVersion,				
								@Version					= @Version,
								@MinorVersion				= @MinorVersion,
								@NotSavedMessage			= @DisplayMessage OUT,
								@CallerInfo					= @CallerInfo,
								@OverrideReasonID			= @OverrideReasonID, -- added BY Shivam Parikh - 07/07/2015 Task : 100644
								@AutoProcessUserID			= @AutoProcessUserID, -- Adde By Shivam Parikh - Task : 103297
								@ValidateCaregiverAbsentOverlap = @ValidateCaregiverAbsentOverlap,
								@SkipAbsenceWarning		    = @SkipAbsenceWarning,
								@VisitChangeData			= @VisitChangeData OUT
						END
					END
						
					IF @IsSaved	< 0 BEGIN						
						ROLLBACK TRAN

						INSERT INTO #FailedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance(VisitInfoAutoID, CallUniqueID, Problem, IsSaved)
						SELECT  @inc,'0',@DisplayMessage, @IsSaved -- @IsSaved added By Shivam Parikh Task : 100644

					END ELSE BEGIN
						COMMIT TRAN

						SELECT	@VisitID = @IsSaved

						SELECT @CallUniqueID = ISNULL(CallUniqueID,'0')
						FROM CallMaintenance WITH(NOLOCK)
						WHERE MaintenanceID  = @MaintenanceID
						AND EVVSource = 'mobileapp'						
								
						INSERT INTO #SuccessedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance(VisitInfoAutoID, VisitID, CallUniqueID)
						SELECT  @inc, @VisitID, ISNULL(@CallUniqueID,'0')								
					END				
				END
			
				IF @VisitID	IS NOT NULL BEGIN
					SELECT	
							@CallInMID	= v.CallInMID,
							@CallOutMID	= v.CallOutMID
					FROM  #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance v
					WHERE AutoID = @inc

					IF @CallInMID IS NOT NULL AND @CallInMID > 0 BEGIN
						EXEC dbo.CAL_LinkVisit
							@UserID = @ActionByUserID, -- Modified By Shivam Parikh - Task : 103297
							@VisitID = @VisitID,
							@MaintenanceID = @CallInMID,
							@IsDifferentAide = 0,
							@AppVersion = @AppVersion,
							@Version = @Version,
							@MinorVersion = @MinorVersion
					END
					IF @CallOutMID IS NOT NULL AND @CallOutMID > 0 BEGIN
						EXEC dbo.CAL_LinkVisit
							@UserID = @ActionByUserID, -- Modified By Shivam Parikh - Task : 103297
							@VisitID = @VisitID,
							@MaintenanceID = @CallOutMID,
							@IsDifferentAide = 0,
							@AppVersion = @AppVersion,
							@Version = @Version,
							@MinorVersion = @MinorVersion
					END		
				END

				IF @AppVersion = 'ENT'	AND @AppVer >= @ENT13000100 AND CAST(@VisitChangeData AS VARCHAR(MAX)) != '' 
				BEGIN
					SET @VisitChangeData.[modify]('delete (/vs/@ProvId)')
					SET @xmlVisitChangeData = @xmlVisitChangeData + REPLACE(
																			REPLACE( 
																					CONVERT(VARCHAR(100), @VisitChangeData), 
																					'<vs>', ''), 
																			'</vs>', ''
																			)
				END	

				SELECT @inc = @inc + 1
			END		
		END ELSE BEGIN -- END OF @ENT06000209
			SELECT @cnt = @CallsXML.query('count(/Visits/Data)').value('.','INT')

			BEGIN TRAN

			WHILE @inc <= @cnt BEGIN

				SELECT @VisitID = NULL,
						@MaintenanceID = 0,
						@CallUniqueID = '0'

				SELECT @ScheduleInfo = @CallsXML.query('/Visits/Data[position()=sql:variable("@inc")]/Visit[1]')
			
				IF DATALENGTH(@ScheduleInfo) > 0 BEGIN
			
					SELECT	
							@IsSkilledSchedule	= d.v.value('@IsSkilledSchedule','INT'),
							@MaintenanceID = CASE WHEN ISNULL(d.v.value('@CallInMID','INT'),0) = 0 THEN d.v.value('@CallOutMID','INT')
													ELSE ISNULL(d.v.value('@CallInMID','INT'),0)
											END
					FROM	@ScheduleInfo.nodes('/Visit') d(v)

					IF @IsSkilledSchedule = 2 BEGIN
						EXEC Visit.SaveScheduledInfo
							@UserID = @UserID,
							@VisitID = -1,
							@Data = @ScheduleInfo,
							@IsSaved  = @IsSaved OUT,
							@UpdateScheduleOverTime = @UpdateScheduleOverTime,
							@UpdateWithAideCompliant = @UpdateWithAideCompliant,
							@UpdateWithNoAuthorization = @UpdateWithNoAuthorization,
							@AppVersion = @AppVersion,
							@Version = @Version,
							@MinorVersion = @MinorVersion
					END ELSE BEGIN
						EXEC Visit.SaveSkilledScheduledInfo
								@UserID	= @UserID,					
								@VisitID = -1,				
								@Data = @ScheduleInfo,				
								@IsSaved = @IsSaved OUT,
								@UpdateScheduleOverTime = @UpdateScheduleOverTime,	
								@UpdateWithAideCompliant = @UpdateWithAideCompliant,
								@UpdateWithNoAuthorization = @UpdateWithNoAuthorization,
								@AppVersion = @AppVersion,				
								@Version = @Version,
								@MinorVersion = @MinorVersion

					END

					IF @IsSaved	< 0 BEGIN
						ROLLBACK TRAN
						RETURN @IsSaved
					END ELSE BEGIN
						SELECT	@VisitID = @IsSaved

						SELECT @CallUniqueID = ISNULL(CallUniqueID,'0')
						FROM CallMaintenance WITH(NOLOCK)
						WHERE MaintenanceID  = @MaintenanceID
						AND EVVSource = 'mobileapp'
					
						IF @CallUniqueID != '0' BEGIN
							INSERT INTO #ReturnValues_dbo_CAL_CreateSchedulesFromCallMaintenance
							(
								VisitID,
								CallUniqueID	
							)
							SELECT	@VisitID,
									@CallUniqueID
						END
					END
				END

				IF @VisitID	IS NOT NULL BEGIN
					SELECT	
							@CallInMID	= d.v.value('@CallInMID','BIGINT'),
							@CallOutMID	= d.v.value('@CallOutMID','BIGINT')
					FROM	@ScheduleInfo.nodes('/Visit') d(v)
				
					IF @CallInMID IS NOT NULL AND @CallInMID > 0 BEGIN
						EXEC dbo.CAL_LinkVisit
							@UserID = @UserID,
							@VisitID = @VisitID,
							@MaintenanceID = @CallInMID,
							@IsDifferentAide = 0,
							@AppVersion = @AppVersion,
							@Version = @Version,
							@MinorVersion = @MinorVersion
					END
					IF @CallOutMID IS NOT NULL AND @CallOutMID > 0 BEGIN
						EXEC dbo.CAL_LinkVisit
							@UserID = @UserID,
							@VisitID = @VisitID,
							@MaintenanceID = @CallOutMID,
							@IsDifferentAide = 0,
							@AppVersion = @AppVersion,
							@Version = @Version,
							@MinorVersion = @MinorVersion
					END		
				END
			
				SELECT @inc = @inc + 1
			END
				
			COMMIT TRAN	
		END -- END OF @ENT06000209 condition

		--Added Distinct code for ENT 6.6.4.3
		IF @AppVersion='ENT' AND @AppVer >= @ENT06060403 AND ISNULL(@SkipSuccessFailureSelection, 0) = 0
        BEGIN
			--===========================================================================================================
			-- Failed Visits Records
			--===========================================================================================================
			SELECT DISTINCT 
				-- space added between Patient Name and Patient Number - Shivam Parikh - 06/18/2015
				p.LastName + ', ' + p.FirstName + ' (' + 
					CASE 
						WHEN @AppVersion='ENT' AND @AppVer  >= @ENT06050507 THEN vp.OfficePatientCode
						ELSE p.PatientNumber
					END +')' AS [Patient], 
				-- end modification - shivam parikh
				CONVERT(VARCHAR(11), v.VisitDate, 101) AS [Date],
				v.SS + ' - ' + v.SE AS [Schedule],
				CASE WHEN p.CHHAID > 0 THEN cm.Chhaname ELSE c.ContractName END AS [Contract],
				s.ServiceCode [Service Code],
				-- space added between Caregiver Name and Caregiver Code - Shivam Parikh - 06/18/2015
				a.LastName + ', ' + a.FirstName + ' (' + 
					CASE 
					WHEN @AppVersion='ENT' AND @AppVer  >= @ENT06050507 THEN ISNULL(NULLIF(a.OfficeAideCode,''), a.AideCode)
					ELSE a.AideCode 
					END 
				+')' AS [Caregiver],
				-- end modification - shivam parikh
				pr.PayRate AS [Pay Code],				
				f.Problem,
				f.IsSaved, -- added BY Shivam Parikh - Task : 100644
		    	p.PatientID,
				a.AideID,
				CASE WHEN p.CHHAID > 0 THEN 0 ELSE 1 END AS IsInternal -- 0 - Payer Patient, 1- Internal Patient
			FROM #FailedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance f
			INNER JOIN #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance v ON v.AutoID = f.VisitInfoAutoID
			INNER JOIN dbo.TblPatientMaster p (NOLOCK) ON p.PatientID = v.PatientID
			INNER JOIN dbo.VendorPatients vp (NOLOCK) ON p.PatientID = vp.PatientID
            LEFT JOIN dbo.tblChhaMaster cm WITH(NOLOCK) ON p.ChhaID = cm.ChhaID
			LEFT JOIN dbo.Contracts c (NOLOCK) ON c.ContractID = v.ContractID1
			LEFT JOIN dbo.ServiceCodes s (NOLOCK) ON s.ServiceCodeID = v.RateID1
	     	LEFT JOIN dbo.TblAideMaster a (NOLOCK) ON a.AideID = v.AideID
			LEFT JOIN dbo.PayRates pr (NOLOCK) ON pr.PayRateID = v.PayRateID
			--===========================================================================================================

			--===========================================================================================================
			-- Success Visits Records
			--===========================================================================================================
			SELECT DISTINCT 
				-- space added between Patient Name and Patient Number - Shivam Parikh - 06/18/2015
				p.LastName + ', ' + p.FirstName + ' (' + 
					CASE 
						WHEN @AppVersion='ENT' AND @AppVer  >= @ENT06050507 THEN vp.OfficePatientCode
						ELSE p.PatientNumber
					END
					+')' AS [Patient],
				-- end modification
				CONVERT(VARCHAR(11), v.VisitDate, 101) AS [Date],
				v.SS + ' - ' + v.SE AS [Schedule],
				CASE WHEN p.CHHAID > 0 THEN cm.Chhaname ELSE c.ContractName END AS [Contract],
				s.ServiceCode [Service Code],
				-- space added between Caregiver Name and Caregiver Code - Shivam Parikh - 06/18/2015
				a.LastName + ', ' + a.FirstName + ' (' + 
					CASE 
						WHEN @AppVersion='ENT' AND @AppVer  >= @ENT06050507 THEN ISNULL(NULLIF(a.OfficeAideCode,''), a.AideCode)
						ELSE a.AideCode 
					END 
				+')' AS [Caregiver],
				-- end modification
				pr.PayRate AS [Pay Code],
				p.PatientID,
				a.AideID,
				CASE WHEN p.CHHAID > 0 THEN 0 ELSE 1 END AS IsInternal, -- 0 - Payer Patient, 1- Internal Patient
				sv.CallUniqueID,
				sv.VisitID,
				v.IsSkilledSchedule AS IsSkilled, -- Added By Shivam Parikh - 06/18/2015 --modified by Darshan Shah on 26/06/2015
				vp.OfficeID
			FROM #SuccessedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance sv
			INNER JOIN #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance v ON v.AutoID = sv.VisitInfoAutoID
			INNER JOIN dbo.TblPatientMaster p (NOLOCK) ON p.PatientID = v.PatientID
			INNER JOIN dbo.VendorPatients vp (NOLOCK) ON p.PatientID = vp.PatientID
            LEFT JOIN dbo.tblChhaMaster cm WITH(NOLOCK) ON p.ChhaID = cm.ChhaID
			LEFT JOIN dbo.Contracts c (NOLOCK) ON c.ContractID = v.ContractID1
			LEFT JOIN dbo.ServiceCodes s (NOLOCK) ON s.ServiceCodeID = v.RateID1
			LEFT JOIN dbo.TblAideMaster a (NOLOCK) ON a.AideID = v.AideID
			LEFT JOIN dbo.PayRates pr (NOLOCK) ON pr.PayRateID = v.PayRateID
			--===========================================================================================================
		END 
		ELSE IF @AppVersion='ENT' AND @AppVer  >= @ENT06000209 AND ISNULL(@SkipSuccessFailureSelection, 0) = 0 -- condition added by Shivam Parikh - 08/18/2015
        BEGIN
			--===========================================================================================================
			-- Failed Visits Records
			--===========================================================================================================
			SELECT 
				-- space added between Patient Name and Patient Number - Shivam Parikh - 06/18/2015
				p.LastName + ', ' + p.FirstName + ' (' + 
					CASE 
						WHEN @AppVersion='ENT' AND @AppVer  >= @ENT06050507 THEN vp.OfficePatientCode
						ELSE p.PatientNumber
					END +')' AS [Patient], 
				-- end modification - shivam parikh
				CONVERT(VARCHAR(11), v.VisitDate, 101) AS [Date],
				v.SS + ' - ' + v.SE AS [Schedule],
				c.ContractName AS [Contract],
				s.ServiceCode [Service Code],
				-- space added between Caregiver Name and Caregiver Code - Shivam Parikh - 06/18/2015
				a.LastName + ', ' + a.FirstName + ' (' + 
					CASE 
					WHEN @AppVersion='ENT' AND @AppVer  >= @ENT06050507 THEN ISNULL(NULLIF(a.OfficeAideCode,''), a.AideCode)
					ELSE a.AideCode 
					END 
				+')' AS [Caregiver],
				-- end modification - shivam parikh
				pr.PayRate AS [Pay Code],				
				f.Problem,
				f.IsSaved, -- added BY Shivam Parikh - Task : 100644
		    	p.PatientID,
				a.AideID,
				CASE WHEN p.CHHAID > 0 THEN 0 ELSE 1 END AS IsInternal -- 0 - Payer Patient, 1- Internal Patient				
			FROM #FailedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance f
			INNER JOIN #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance v ON v.AutoID = f.VisitInfoAutoID
			INNER JOIN dbo.TblPatientMaster p (NOLOCK) ON p.PatientID = v.PatientID
			INNER JOIN dbo.VendorPatients vp (NOLOCK) ON p.PatientID = vp.PatientID
			LEFT JOIN dbo.Contracts c (NOLOCK) ON c.ContractID = v.ContractID1
			LEFT JOIN dbo.ServiceCodes s (NOLOCK) ON s.ServiceCodeID = v.RateID1
	     	LEFT JOIN dbo.TblAideMaster a (NOLOCK) ON a.AideID = v.AideID
			LEFT JOIN dbo.PayRates pr (NOLOCK) ON pr.PayRateID = v.PayRateID
			--===========================================================================================================

			--===========================================================================================================
			-- Success Visits Records
			--===========================================================================================================
			SELECT 
				-- space added between Patient Name and Patient Number - Shivam Parikh - 06/18/2015
				p.LastName + ', ' + p.FirstName + ' (' + 
					CASE 
						WHEN @AppVersion='ENT' AND @AppVer  >= @ENT06050507 THEN vp.OfficePatientCode
						ELSE p.PatientNumber
					END
					+')' AS [Patient],
				-- end modification
				CONVERT(VARCHAR(11), v.VisitDate, 101) AS [Date],
				v.SS + ' - ' + v.SE AS [Schedule],
				c.ContractName AS [Contract],
				s.ServiceCode [Service Code],
				-- space added between Caregiver Name and Caregiver Code - Shivam Parikh - 06/18/2015
				a.LastName + ', ' + a.FirstName + ' (' + 
					CASE 
						WHEN @AppVersion='ENT' AND @AppVer  >= @ENT06050507 THEN ISNULL(NULLIF(a.OfficeAideCode,''), a.AideCode)
						ELSE a.AideCode 
					END 
				+')' AS [Caregiver],
				-- end modification
				pr.PayRate AS [Pay Code],
				p.PatientID,
				a.AideID,
				CASE WHEN p.CHHAID > 0 THEN 0 ELSE 1 END AS IsInternal, -- 0 - Payer Patient, 1- Internal Patient
				sv.CallUniqueID,
				sv.VisitID,
				v.IsSkilledSchedule AS IsSkilled, -- Added By Shivam Parikh - 06/18/2015 --modified by Darshan Shah on 26/06/2015
				vp.OfficeID
			FROM #SuccessedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance sv
			INNER JOIN #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance v ON v.AutoID = sv.VisitInfoAutoID
			INNER JOIN dbo.TblPatientMaster p (NOLOCK) ON p.PatientID = v.PatientID
			INNER JOIN dbo.VendorPatients vp (NOLOCK) ON p.PatientID = vp.PatientID
			LEFT JOIN dbo.Contracts c (NOLOCK) ON c.ContractID = v.ContractID1
			LEFT JOIN dbo.ServiceCodes s (NOLOCK) ON s.ServiceCodeID = v.RateID1
			LEFT JOIN dbo.TblAideMaster a (NOLOCK) ON a.AideID = v.AideID
			LEFT JOIN dbo.PayRates pr (NOLOCK) ON pr.PayRateID = v.PayRateID
			--===========================================================================================================
		END ELSE IF @AppVer  >= @ENT06000200 BEGIN
			SELECT	VisitID,
				ISNULL(CallUniqueID,'0') AS CallUniqueID
			FROM #ReturnValues_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		--===========================================================================================================
		-- Drop Temp Tables
		--===========================================================================================================

		--Added By: Maulik Suthar_ENTML-20034_08082018
			IF @AppVersion = 'ENT' AND @AppVer >= @ENT13000100 AND @xmlVisitChangeData != '' 
			BEGIN
				SET @VisitChangeData = CONVERT(XML, '<vs ProvId = "' + CONVERT(varchar,@CompanyID) + '">' + @xmlVisitChangeData +'</vs>')
			END

		IF OBJECT_ID('tempdb..#ReturnValues_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #ReturnValues_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#SuccessedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #SuccessedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#FailedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #FailedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#VisitInfoDetailUnique_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #VisitInfoDetailUnique_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		--===========================================================================================================
	END TRY	
	BEGIN CATCH

		IF @@TRANCOUNT > 0 BEGIN
			ROLLBACK TRAN
		END
		--===========================================================================================================
		-- Drop Temp Tables
		--===========================================================================================================
		IF OBJECT_ID('tempdb..#ReturnValues_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #ReturnValues_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#SuccessedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #SuccessedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#FailedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #FailedVisits_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #VisitInfoDetail_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		IF OBJECT_ID('tempdb..#VisitInfoDetailUnique_dbo_CAL_CreateSchedulesFromCallMaintenance') IS NOT NULL
		BEGIN
			DROP TABLE #VisitInfoDetailUnique_dbo_CAL_CreateSchedulesFromCallMaintenance
		END
		--===========================================================================================================

		DECLARE @Param XML
		SELECT @Param = ( 
			SELECT
				'[dbo].[CAL_CreateSchedulesFromCallMaintenance]' AS '@ProcName', 
				@UserID						AS '@UserID',
				CAST(@CallsXML AS VARCHAR(8000))	AS '@CallsXML',
				@UpdateWithNoAuthorization	AS '@UpdateWithNoAuthorization',
				@UpdateScheduleOverTime		AS '@UpdateScheduleOverTime',
				@UpdateWithAideCompliant	AS '@UpdateWithAideCompliant',
				@IsSaved					AS '@IsSaved',
				@AppVersion					AS '@AppVersion',
				@Version					AS '@Version',
				@MinorVersion				AS '@MinorVersion',
				@CallerInfo					AS '@CallerInfo',
				@IncludeMultipleVisits      AS '@IncludeMultipleVisits',
				@OverrideReasonID			AS '@OverrideReasonID', -- Added by - Shivam Parikh - 06/18/2015 Task : 100644
				@AutoProcessUserID			AS '@AutoProcessUserID' -- Adde By Shivam Parikh - Task : 103297
			FOR XML PATH('Params') 
		)
		EXEC ERR_LogError @Params = @Param
		;THROW		
	END CATCH
END

