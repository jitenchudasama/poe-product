
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/****** Object:  StoredProcedure [EVV].[AutoCreateVisitBasedOnEVVConfirmation_ENT18060100]    Script Date: 27-02-2019 15:06:57 ******/

-- =============================================
-- Created By		: Chandrakant
-- Created date		: 16 November 2019
-- Version		: 18.6.1.0 
-- Reference SP		: [EVV].[AutoCreateVisitBasedOnEVVConfirmation_06060505]
-- =================================================================
/*
EXEC [EVV].[AutoCreateVisitBasedOnEVVConfirmation_ENT18060100]
	@AssignmentID		= '998765',
	@CallerID			= '1111110000',
	@CallType			= 'O',
	@AgencyID			= 92,
	@CallUniqueID		= NULL,
	@EVVSource			= 'MOBILEAPP',
	@EVVType			= 'FOB',
	@PatientID			= 942773,
	@EVVDeviceID		= '00100',
	@CallMaintenanceIDO	= 11880
*/

CREATE PROCEDURE [EVV].[AutoCreateVisitBasedOnEVVConfirmation_ENT18060100]
(
	@AssignmentID			VARCHAR(6),	--@AideCode, to check whether ChhaInitial is available or not
	@CallerID				VARCHAR(50),
	@CallType				VARCHAR(4),
	@AgencyID				INT,
	@CallUniqueID			VARCHAR(50),
	@EVVSource				VARCHAR(10),
	@EVVType				VARCHAR(10),
	@PatientID				BIGINT,
	@EVVDeviceID			VARCHAR(10),
	@CallMaintenanceIDO		INT,
	@UserID					INT,
	@OfficeID				INT,
	@Version				DECIMAL(4,2),
	@MinorVersion			DECIMAL(4,2),
	@RateID1				INT,
	@ContractID				INT,
	@PatientHomePhone		VARCHAR(100),
	@PatientHomePhone2		VARCHAR(100),
	@PatientHomePhone3		VARCHAR(100),
	@CallerInfo				VARCHAR(MAX),
	@AppVersion				VARCHAR(10)
)
AS
BEGIN
	SET NOCOUNT ON
	SET XACT_ABORT ON;

	BEGIN TRY
	IF ISNULL(@UserID,0) = 0 BEGIN
		RAISERROR('Error in %s: %s', 16, 1, '[EVV].[AutoCreateVisitBasedOnEVVConfirmation_ENT18060100]', 'UserID should not be blank.')
		RETURN
    END

	EXEC SetContextInfoUserID @UserID = @UserID
	
		DECLARE @CaregiverID INT,
				@IsSkilledSchedule INT,
				@CallsXML					XML,
				@UpdateWithNoAuthorization	INT = -1,
				@UpdateScheduleOverTime		INT = 0,
				@UpdateWithAideCompliant	INT = 0,
				@IsSaved					INT = 0,				
				@OverrideReasonID			INT = -1,
				@PayRateID					INT = -1,				
				@IncludeMultipleVisits 		INT = 0,
				@Date 						DATETIME,
				@SS 						VARCHAR(10),
				@SE 						VARCHAR(10),
				@SSDateTeTime				VARCHAR(MAX),
				@SEDateTeTime				VARCHAR(MAX),
				@Hours1 					INT,
				@Minutes1 					INT,
				@CallInMID 					INT,
				@CallOutMID 				INT,
				@AutoScheduleServiceTypeID 	VARCHAR(10),
				@VisitDate 					DATETIME,
				@AutoScheduleServiceTypePriContractID INT = 0,
				@NotRoundingVisitTime		BIT = 0,
				@ValidatedContactID 		INT = 0,
				@ValidatedServiceCodeID 	INT = 0,
				@Result 					INT = 0

		SELECT @CaregiverID = AM.AideID
		FROM dbo.tblAideMaster AM WITH(NOLOCK)
		WHERE AM.IVRAideVendorID = @AssignmentID
			AND AM.VendorID = @AgencyID

		SELECT TOP 1 @PayRateID = s.PayRateID 
		FROM dbo.PayRates s (NOLOCK)
		INNER JOIN dbo.Disciplines d (NOLOCK) ON s.[DisciplineID] = d.[DisciplineID]
		INNER JOIN  aide.Disciplines ad WITH (NOLOCK) ON ad.DisciplineID = d.DisciplineID
			AND ad.AideID = @CaregiverID
			AND s.Active = 1		   -- Active pyarate
			AND s.IsDefaultRate = 1	-- Default rate only
			AND s.CompanyID = @AgencyID
		INNER JOIN Office.Payrolls p (NOLOCK) ON p.PayrollSetupID = s.PayrollSetupID 
			AND p.Deleted = 0 
			AND p.OfficeID = @OfficeID
		INNER JOIN dbo.DisciplineRates dr (NOLOCK ) ON d.DisciplineID = dr.Discipline		
			AND s.PayrateID = dr.ServiceCodeID	
			AND dr.CompanyID = s.CompanyID
		ORDER BY CASE WHEN ad.DisciplineID = 1 THEN 10 ELSE ad.DisciplineID END
		
		--=========================================================	

		--==========================================================
		-- Get is skilled visit or non skilled visit
		--==========================================================
		DECLARE @DisciplineID INT,
					@ServiceCodeDisciplineType INT,
					@ServiceCodeNotMatch INT

		IF ISNULL(@RateID1,-1) <> -1
		BEGIN
			SELECT @DisciplineID = sc.DisciplineID ,
					@ServiceCodeDisciplineType = d.DisciplineType,
					@IsSkilledSchedule =  d.DisciplineType
			FROM dbo.ServiceCodes sc (NOLOCK)
			INNER JOIN dbo.Disciplines d (NOLOCK) ON d.DisciplineID =  sc.DisciplineID
			WHERE ServiceCodeID = @RateID1
		END		

		--SELECT sc.DisciplineID ,
		--	   d.DisciplineType,
		--	   d.DisciplineType
		--	FROM dbo.ServiceCodes sc (NOLOCK)
		--	INNER JOIN dbo.Disciplines d (NOLOCK) ON d.DisciplineID =  sc.DisciplineID
		--	WHERE ServiceCodeID = 172407


		
		--==========================================================

		--==========================================================
		-- Query to get CMI and CMO data for Auto creation of Visit based on EVV Confirmztion
		--==========================================================
		IF (@CaregiverID > 0 AND @PatientID > 0 AND @PayRateID > 0 AND @IsSkilledSchedule > 0)
		BEGIN

			IF EXISTS(
					SELECT TOP 1 1
					FROM dbo.TblPatientMaster(NOLOCK)
					WHERE PatientID = @PatientID
						AND ChhaID IS NULL
					)
			BEGIN
				SELECT	TOP 1
				@NotRoundingVisitTime = cv.NotRoundingVisitTime
				FROM dbo.Contracts c (NOLOCK) 
				INNER JOIN dbo.ChhaVendors cv (NOLOCK) on cv.ChhaID = c.ChhaID
					AND cv.VendorID = @AgencyID
					AND c.ContractID = @ContractID					
			END
			ELSE
			BEGIN
				SELECT	TOP 1
				@NotRoundingVisitTime = cv.NotRoundingVisitTime
				FROM dbo.ChhaVendors cv (NOLOCK) 
				WHERE cv.ChhaID = @ContractID
					AND cv.VendorID = @AgencyID
			END

			DECLARE @POCHeaderID INT = NULL
			SELECT	
				TOP (1) 
				@POCHeaderID = ph.POCHeaderID
			FROM dbo.CallMaintenance cmo (NOLOCK)
			INNER JOIN dbo.TblPOCHeader ph (NOLOCK) ON
				CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN CAST(cmo.EVVCalltime AS DATE) ELSE CAST(cmo.Calltime AS DATE)  END 
				BETWEEN  CAST(ISNULL(ph.StartDate,GETDATE()) AS DATE) AND CAST(ISNULL(ph.EndDate, '3075-12-31') AS DATE)
			WHERE ph.PatientID = @PatientID
			AND @IsSkilledSchedule = 2 -- for non-skilled visits only
			AND cmo.MaintenanceID = @CallMaintenanceIDO
			ORDER BY ph.StartDate DESC, ph.CreatedDate DESC

			IF @AgencyID IN (92, 93, 866, 887, 917, 848, 430, 914, 595, 744, 691, 332, 825, 568)
			BEGIN
				IF @NotRoundingVisitTime = 0
                BEGIN
                    SELECT TOP 1
					    @Date= CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN CAST(cmi.EVVCalltime AS DATE) ELSE CAST(cmi.Calltime AS DATE)  END,
					    @SS = FORMAT(CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, 'HHmm'), -- hhmm replaced with HHmm by Shivam Parikh - 08/28/2015
					    @SE = FORMAT(CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END, 'HHmm'), -- hhmm replaced with HHmm by Shivam Parikh - 08/28/2015				
					    @Hours1 = DATEDIFF(MINUTE, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END) / 60,
					    @Minutes1 = DATEDIFF(MINUTE, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END) % 60,				
					    @CallInMID = cmi.MaintenanceID,
					    @CallOutMID = cmo.MaintenanceID,
					    @AutoScheduleServiceTypeID = ISNULL(cmo.AutoScheduleServiceTypeID, '')
				    FROM dbo.CallMaintenance cmi WITH (NOLOCK)
				    INNER JOIN dbo.CallMaintenance cmo WITH (NOLOCK) ON cmi.VendorID = cmo.VendorID
					    AND @CallMaintenanceIDO IN (cmi.MaintenanceID, cmo.MaintenanceID)
					    AND cmi.CallType = 'I'
					    AND cmo.CallType = 'O'
					    AND cmi.VendorID = @AgencyID
					    AND cmi.StatusID IN (8, 22)
					    AND cmo.StatusID IN (8, 22)
					    AND cmi.VendorID = cmo.VendorID
					    AND cmi.AssignmentID = cmo.AssignmentID					
					    --AND cmi.AideID = cmo.AideID
					    AND	cmi.EVVPatientId = cmo.EVVPatientId
					    AND CASE WHEN ISNULL(CMO.EvvType, 'CALLERID') = 'FOB' THEN cmo.EVVCalltime ELSE cmo.Calltime END >= CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END
					    AND CASE WHEN CMO.EvvType = 'FOB' THEN CAST(cmo.EVVCallTime AS DATE) ELSE CAST(cmo.CallTime AS DATE) END
								    = CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN CAST(cmi.EVVCallTime AS DATE) ELSE CAST(cmi.CallTime AS DATE) END -- for same day only
					    AND DATEDIFF(MINUTE, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime ELSE cmi.Calltime END, CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime ELSE cmo.Calltime END) <> 0
				    ORDER BY DATEDIFF(MI, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(CMO.EvvType, 'CALLERID') = 'FOB' THEN CAST(cmo.EVVCallTime AS DATE) ELSE CAST(cmo.CallTime AS DATE) END)
                END
                ELSE
                BEGIN
                    SELECT TOP 1
					    @Date= CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN CAST(cmi.EVVCalltime AS DATE) ELSE CAST(cmi.Calltime AS DATE)  END,
					    @SS = FORMAT(CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, 'HHmm'), -- hhmm replaced with HHmm by Shivam Parikh - 08/28/2015
					    @SE = FORMAT(CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END, 'HHmm'), -- hhmm replaced with HHmm by Shivam Parikh - 08/28/2015				
					    @Hours1 = DATEDIFF(MINUTE, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END) / 60,
					    @Minutes1 = DATEDIFF(MINUTE, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END) % 60,				
					    @CallInMID = cmi.MaintenanceID,
					    @CallOutMID = cmo.MaintenanceID,
					    @AutoScheduleServiceTypeID = ISNULL(cmo.AutoScheduleServiceTypeID, '')
				    FROM dbo.CallMaintenance cmi WITH (NOLOCK)
				    INNER JOIN dbo.CallMaintenance cmo WITH (NOLOCK) ON cmi.VendorID = cmo.VendorID
					    AND @CallMaintenanceIDO IN (cmi.MaintenanceID, cmo.MaintenanceID)
					    AND cmi.CallType = 'I'
					    AND cmo.CallType = 'O'
					    AND cmi.VendorID = @AgencyID
					    AND cmi.StatusID IN (8, 22)
					    AND cmo.StatusID IN (8, 22)
					    AND cmi.VendorID = cmo.VendorID
					    AND cmi.AssignmentID = cmo.AssignmentID					
					    --AND cmi.AideID = cmo.AideID
					    AND	cmi.EVVPatientId = cmo.EVVPatientId
					    AND CASE WHEN ISNULL(CMO.EvvType, 'CALLERID') = 'FOB' THEN cmo.EVVCalltime ELSE cmo.Calltime END >= CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END
					    AND CASE WHEN CMO.EvvType = 'FOB' THEN CAST(cmo.EVVCallTime AS DATE) ELSE CAST(cmo.CallTime AS DATE) END
								    = CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN CAST(cmi.EVVCallTime AS DATE) ELSE CAST(cmi.CallTime AS DATE) END -- for same day only
					    AND DATEDIFF(MINUTE, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN dbo.GetRoundDateTime(cmi.EVVCalltime) ELSE dbo.GetRoundDateTime(cmi.Calltime) END, CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN dbo.GetRoundDateTime(cmo.EVVCalltime) ELSE dbo.GetRoundDateTime(cmo.Calltime) END) <> 0
				    ORDER BY DATEDIFF(MI, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(CMO.EvvType, 'CALLERID') = 'FOB' THEN CAST(cmo.EVVCallTime AS DATE) ELSE CAST(cmo.CallTime AS DATE) END)
                END
			END
			ELSE
			BEGIN
				SELECT TOP 1
					@Date= CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN CAST(cmi.EVVCalltime AS DATE) ELSE CAST(cmi.Calltime AS DATE)  END,
					@SS = FORMAT(CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, 'HHmm'), -- hhmm replaced with HHmm by Shivam Parikh - 08/28/2015
					@SE = FORMAT(CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END, 'HHmm'), -- hhmm replaced with HHmm by Shivam Parikh - 08/28/2015				
					@Hours1 = DATEDIFF(MINUTE, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END) / 60,
					@Minutes1 = DATEDIFF(MINUTE, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END) % 60,				
					@CallInMID = cmi.MaintenanceID,
					@CallOutMID = cmo.MaintenanceID,
					@AutoScheduleServiceTypeID = ISNULL(cmo.AutoScheduleServiceTypeID, '')
				FROM dbo.CallMaintenance cmi WITH (NOLOCK)
				INNER JOIN dbo.CallMaintenance cmo WITH (NOLOCK) ON cmi.VendorID = cmo.VendorID
					AND @CallMaintenanceIDO IN (cmi.MaintenanceID, cmo.MaintenanceID)
					AND cmi.CallType = 'I'
					AND cmo.CallType = 'O'
					AND cmi.VendorID = @AgencyID
					AND cmi.StatusID IN (8, 22)
					AND cmo.StatusID IN (8, 22)
					AND cmi.VendorID = cmo.VendorID
					AND cmi.AssignmentID = cmo.AssignmentID
					--AND cmi.AideID = cmo.AideID
					AND	cmi.EVVPatientId = cmo.EVVPatientId
					AND CASE WHEN ISNULL(CMO.EvvType, 'CALLERID') = 'FOB' THEN cmo.EVVCalltime ELSE cmo.Calltime END >= CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END
					AND CASE WHEN CMO.EvvType = 'FOB' THEN CAST(cmo.EVVCallTime AS DATE) ELSE CAST(cmo.CallTime AS DATE) END
								= CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN CAST(cmi.EVVCallTime AS DATE) ELSE CAST(cmi.CallTime AS DATE) END -- for same day only
				ORDER BY DATEDIFF(MI, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(CMO.EvvType, 'CALLERID') = 'FOB' THEN CAST(cmo.EVVCallTime AS DATE) ELSE CAST(cmo.CallTime AS DATE) END)
			END

			SET @VisitDate = CAST(@Date AS DATE)

			SELECT 
				@AutoScheduleServiceTypePriContractID = ISNULL(sc.ContractID,0)
			FROM dbo.ServiceCodes sc WITH (NOLOCK)
			INNER JOIN dbo.PatientContracts pc (NOLOCK) ON pc.PatientID = @PatientID
				AND pc.ContractID = sc.ContractID 
				AND pc.IsPrimaryContract = 1  -- Is Primary contract
			WHERE sc.CompanyID = @AgencyID
				AND sc.CompanyType = 'V'
				AND sc.AutoScheduleServiceTypeID = @AutoScheduleServiceTypeID
			
			IF @AutoScheduleServiceTypeID <> '' AND @AutoScheduleServiceTypePriContractID > 0 
			BEGIN
							
				EXECUTE [dbo].[ValidateAutoScheduleServiceTypeID]
					@UserID = @UserID,
					@VisitID = -1, -- New Visit
					@ProviderID = @AgencyID,
					@AutoScheduleServiceTypeID = @AutoScheduleServiceTypeID,
					@PatientID = @PatientID,					
					@VisitDate = @VisitDate,					
					@AideDisciplineType = @IsSkilledSchedule,
					@AppVersion = @AppVersion,
					@Version = @Version,
					@MinorVersion = @MinorVersion,
					@CallerInfo = @CallerInfo,
					@ContactID = @ValidatedContactID OUT,
					@ServiceCodeID = @ValidatedServiceCodeID OUT,
					@Result = @Result OUT

				IF @Result = 1 -- Service Type is validated
				BEGIN	
					
					SELECT @ContractID = @ValidatedContactID
						   
					IF @RateID1 <> @ValidatedServiceCodeID	
					BEGIN
						SELECT @RateID1 = @ValidatedServiceCodeID	
						SELECT @DisciplineID = sc.DisciplineID ,
								@ServiceCodeDisciplineType = d.DisciplineType,
								@IsSkilledSchedule =  d.DisciplineType
						FROM dbo.ServiceCodes sc (NOLOCK)
						INNER JOIN dbo.Disciplines d (NOLOCK) ON d.DisciplineID =  sc.DisciplineID
						WHERE ServiceCodeID = @RateID1
					END						
							
				END	

			END	
					
			IF @CallInMID IS NULL AND @CallOutMID IS NULL -- means no record matches
			BEGIN
				SELECT @CallsXML = NULL
			END ELSE
			BEGIN
				SELECT @CallsXML = '<Visits>' + (
					SELECT 
						@Date AS [Date],
						@SS AS SS, -- hhmm replaced with HHmm by Shivam Parikh - 08/28/2015
						@SE AS SE, -- hhmm replaced with HHmm by Shivam Parikh - 08/28/2015
						@CaregiverID AS AideID,
						@PatientID AS PatientID,
						@PayRateID AS PayRateID,
						@ContractID AS ContractID1,
						@RateID1 AS RateID1,
						@Hours1 AS Hours1,
						@Minutes1 AS Minutes1,
						@POCHeaderID AS POC, 
						0 AS IsTempSchedule,
						0 AS IsTempAide,
						@CallInMID AS CallInMID,
						@CallOutMID AS CallOutMID,
						@IsSkilledSchedule AS IsSkilledSchedule			
				FOR XML RAW ('Visit'), ROOT ('Data')
				) + '</Visits>'
			END
			--==========================================================

			--==========================================================================
			--Get Support USer from AgencyID
			--==========================================================================
			DECLARE @SupportUserID INT

			SELECT TOP 1 @SupportUserID = um.UserID 
			FROM TblUserMaster um (NOLOCK) 
			WHERE um.ID = @AgencyID
			AND um.IsSupportUser = 1
			AND um.IsActive = 1
			AND um.UserMasterRole = 'VENDOR'
			--==========================================================================

			--==========================================================================
			-- Execute the existing automatic schedule creation script
			--==========================================================================
			EXEC [dbo].[CAL_CreateSchedulesFromCallMaintenance]
				@UserID							= @SupportUserID, -- replace with actual userid
				@CallsXML						= @CallsXML,
				@UpdateWithNoAuthorization		= @UpdateWithNoAuthorization,
				@UpdateScheduleOverTime			= @UpdateScheduleOverTime,
				@UpdateWithAideCompliant		= @UpdateWithAideCompliant,
				@IsSaved						= @IsSaved OUT,
				@AppVersion						= @AppVersion,
				@Version						= @Version,
				@MinorVersion					= @MinorVersion,
				@CallerInfo						= @CallerInfo,
				@IncludeMultipleVisits			= @IncludeMultipleVisits,
				@OverrideReasonID				= @OverrideReasonID,
				@AutoProcessUserID				= @UserID,
				@SkipSuccessFailureSelection	= 1, 
				@ValidateCaregiverAbsentOverlap	= 1,
				@SkipAbsenceWarning				= 1
			--=============================================================

			--==========================================================
			-- exception codes
			--==========================================================
				
			IF @IsSkilledSchedule <> @ServiceCodeDisciplineType BEGIN
				SET @ServiceCodeNotMatch = 1
				--SET @DefaltServiceCodeID = -1
			END	
			

		    IF @ServiceCodeNotMatch = 1
            BEGIN
				INSERT INTO ProcessException
				(
					ProcessName,
					CallerID,
					AssignmentID, 
					CallTime,
					Message,
					CallInMID,
					CallOutMID
				)
				SELECT 
					'[EVV].[AutoCreateVisitBasedOnEVVConfirmation_ENT18060100]',
					@CallerID,
					@AssignmentID,
					T.c.value('@Date', 'DATETIME'),
					'Default ServiceCode Discipline Type does not match with Aide Discipline Type',
					T.c.value('@CallInMID', 'INT'),
					T.c.value('@CallOutMID', 'INT')
					FROM @CallsXML.nodes('/Visits/Data/*') T(c)

		    END 
            ELSE IF ISNULL(@RateID1,-1) = -1 
			BEGIN
				-- Default Service Code Is Not Set ---
					INSERT INTO ProcessException
					(
						ProcessName,
						CallerID,
						AssignmentID, 
						CallTime,
						Message,
						CallInMID,
						CallOutMID
					)
					SELECT
						'[EVV].[AutoCreateVisitBasedOnEVVConfirmation_ENT18060100]',
						@CallerID,
						@AssignmentID,
						T.c.value('@Date', 'DATETIME'),
						'Default Service Code is not set',
						T.c.value('@CallInMID', 'INT'),
						T.c.value('@CallOutMID', 'INT')
						FROM @CallsXML.nodes('/Visits/Data/*') T(c)
							
				----END ELSE IF dbo.IsOverlapVisitWithPatient(@PatientID, @VisitDate, @SS, @SE, -1, @DisciplineID) = 1
		    END 
            ELSE IF EXISTS(	
						---- Added By Ritesh M, TaskID : 85515 - Auto Schedule Creating duplicate Visits - Do not check 'Allow Shift Overlap' flag
						SELECT	TOP 1 1
						FROM	dbo.tblVisits v (NOLOCK)
						INNER JOIN dbo.VisitContracts vc (NOLOCK) ON vc.VisitID = v.VisitID
							AND vc.BillType = 1
							AND v.VendorID = @AgencyID
						INNER JOIN dbo.ServiceCodes s (NOLOCK) ON s.ServiceCodeID = vc.VisitRateID
						INNER JOIN dbo.Disciplines d WITH (NOLOCK) ON d.DisciplineID = s.DisciplineID
							AND d.DisciplineType = @ServiceCodeDisciplineType
						WHERE	v.PatientID = @PatientID
						AND		v.Deleted = 0	
						AND		v.VisitDate BETWEEN @CallsXML.value('(/Visits/Data/Visit/@Date)[1]', 'DATETIME') - 1 AND @CallsXML.value('(/Visits/Data/Visit/@Date)[1]', 'DATETIME') + 1
						AND		v.ScheduledEndTime > @CallsXML.value('(/Visits/Data/Visit/@Date)[1]', 'DATETIME')
						AND		v.ScheduledStartTime < @CallsXML.value('(/Visits/Data/Visit/@Date)[1]', 'DATETIME')
						AND		v.VisitID <> -1
						AND @CallsXML.value('(/Visits/Data/Visit/@Date)[1]', 'DATETIME') IS NOT NULL
			    )
            BEGIN
				-- schedule is already there
					INSERT INTO ProcessException
					(
						ProcessName,
						CallerID,
						AssignmentID, 
						CallTime,
						Message,
						CallInMID,
						CallOutMID
					)
					SELECT
						'[EVV].[AutoCreateVisitBasedOnEVVConfirmation_ENT18060100]',
						@CallerID,
						@AssignmentID,
						T.c.value('@Date', 'DATETIME'),
						'Schedule is already there.',
						T.c.value('@CallInMID', 'INT'),
						T.c.value('@CallOutMID', 'INT')
						FROM @CallsXML.nodes('/Visits/Data/*') T(c)
			END
		END		
		
	END TRY
	BEGIN CATCH		
		
		DECLARE @Param XML
		SELECT @Param = ( 
			SELECT
				'[EVV].[AutoCreateVisitBasedOnEVVConfirmation_ENT18060100]' AS '@ProcName', 
				@AssignmentID		AS '@AssignmentID',
				@CallerID			AS '@CallerID',
				@CallType			AS '@CallType',
				@AgencyID			AS '@AgencyID',
				@CallUniqueID		AS '@CallUniqueID',
				@EVVSource			AS '@EVVSource',
				@EVVType			AS '@EVVType',
				@PatientID			AS '@PatientID',
				@EVVDeviceID		AS '@EVVDeviceID',
				@CallMaintenanceIDO	AS '@CallMaintenanceIDO',
				@UserID				AS '@UserID',
				@OfficeID				AS '@OfficeID',
				@Version				AS '@Version',
				@MinorVersion			AS '@MinorVersion',
				@RateID1				AS '@RateID1',
				@ContractID				AS '@ContractID',
				@PatientHomePhone		AS '@PatientHomePhone',
				@PatientHomePhone2		AS '@PatientHomePhone2',
				@PatientHomePhone3		AS '@PatientHomePhone3',
				@CallerInfo				AS '@CallerInfo',
				@Appversion				AS '@Appversion'
			FOR XML PATH('Params') 
		)
		EXEC ERR_LogError @Params = @Param
		;THROW
	END CATCH
END


