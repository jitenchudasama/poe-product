SELECT TOP 1
	CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN CAST(cmi.EVVCalltime AS DATE) ELSE CAST(cmi.Calltime AS DATE)  END,
	FORMAT(CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, 'HHmm'), -- hhmm replaced with HHmm by Shivam Parikh - 08/28/2015
	FORMAT(CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END, 'HHmm'), -- hhmm replaced with HHmm by Shivam Parikh - 08/28/2015				
	DATEDIFF(MINUTE, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END) / 60,
	DATEDIFF(MINUTE, CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END, CASE WHEN ISNULL(cmo.EvvType, 'CALLERID') ='FOB' THEN cmo.EVVCalltime  ELSE cmo.Calltime  END) % 60,				
	cmi.MaintenanceID,
	cmo.MaintenanceID,
	ISNULL(cmo.AutoScheduleServiceTypeID, '') AS AutoScheduleServiceTypeID--,cmi.*
FROM dbo.CallMaintenance cmi WITH (NOLOCK)
INNER JOIN dbo.CallMaintenance cmo WITH (NOLOCK) ON cmi.VendorID = cmo.VendorID
	AND 84852762 IN (cmi.MaintenanceID, cmo.MaintenanceID)
	AND cmi.CallType = 'I'
	AND cmo.CallType = 'O'
	AND cmi.VendorID = 964
	AND cmi.StatusID IN (8, 22)
	AND cmo.StatusID IN (8, 22)
	AND cmi.VendorID = cmo.VendorID 
	AND cmi.AssignmentID = cmo.AssignmentID
	--AND cmi.AideID = cmo.AideID
	AND	cmi.EVVPatientId = cmo.EVVPatientId
	AND cmi.AssignmentID = 104170
	AND CASE WHEN ISNULL(CMO.EvvType, 'CALLERID') = 'FOB' THEN cmo.EVVCalltime ELSE cmo.Calltime END >= CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN cmi.EVVCalltime  ELSE cmi.Calltime  END
	AND CASE WHEN CMO.EvvType = 'FOB' THEN CAST(cmo.EVVCallTime AS DATE) ELSE CAST(cmo.CallTime AS DATE) END
				= CASE WHEN ISNULL(cmi.EvvType, 'CALLERID') ='FOB' THEN CAST(cmi.EVVCallTime AS DATE) ELSE CAST(cmi.CallTime AS DATE) END -- for same day only


				SELECT TOP 1 s.PayRateID 
		FROM dbo.PayRates s (NOLOCK)
		INNER JOIN dbo.Disciplines d (NOLOCK) ON s.[DisciplineID] = d.[DisciplineID]
		INNER JOIN  aide.Disciplines ad WITH (NOLOCK) ON ad.DisciplineID = d.DisciplineID
			AND ad.AideID = 2051407
			AND s.Active = 1		   -- Active pyarate
			AND s.IsDefaultRate = 1	-- Default rate only
			AND s.CompanyID = 964
		INNER JOIN Office.Payrolls p (NOLOCK) ON p.PayrollSetupID = s.PayrollSetupID 
			AND p.Deleted = 0 
			AND p.OfficeID = 6605
		INNER JOIN dbo.DisciplineRates dr (NOLOCK ) ON d.DisciplineID = dr.Discipline		
			AND s.PayrateID = dr.ServiceCodeID	
			AND dr.CompanyID = s.CompanyID
		ORDER BY CASE WHEN ad.DisciplineID = 1 THEN 10 ELSE ad.DisciplineID END



			SELECT TOP 1
				pm.PatientID,
				vp.OfficeID,
				pm.PatientHomePhone,
				pm.PatientHomePhone2,
				pm.PatientHomePhone3,				
				a.ServiceCodeID,
				pc.ContractID
	FROM TblPatientMaster pm (NOLOCK)
			INNER JOIN dbo.VendorPatients vp (NOLOCK) ON vp.PatientID = pm.PatientID 
				AND vp.VendorID = 964
				--AND pm.PatientID = 10129855
				AND '3144613011' IN (pm.PatientHomePhone,pm.PatientHomePhone2, pm.PatientHomePhone3)
				AND pm.ChhaID IS NULL		-- Internal Ptients only
				AND vp.VendorPatientStatus = 3 -- active patient
				AND ISNULL(vp.DELETED,0) = 0 -- not deleted			
				AND ISNULL(vp.DisableAutoCreateVisitBasedOnEVV, 'False') <> 'True'
			INNER JOIN dbo.PatientContracts pc (NOLOCK) ON pc.PatientID = pm.PatientID
				AND pc.IsPrimaryContract = 1 -- added by Shivam Parikh - 08/19/2015
			INNER JOIN dbo.ChhaVendors cv WITH (NOLOCK) ON pc.ContractChhaID = cv.ChhaID
				AND cv.VendorID = vp.VendorID
				AND cv.AutoCreateVisitBasedOnEVVConfirmation = 1 -- Auto Create visit based on Evv confirmation must be enabled per contracttion
			INNER JOIN [dbo].[Admissions] a (NOLOCK) ON a.PatientID = pm.PatientID
				AND a.ContractID = pc.ContractID -- Resolved by Shivam Parikh - 08/18/2015
				AND '05-20-2020' BETWEEN a.StartDate AND ISNULL(a.DischargeDate, '2075-12-31')
				ORDER BY a.StartDate DESC