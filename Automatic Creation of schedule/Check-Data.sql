SELECT cmi.CallType,cmo.CallType, cmi.CallTime,cmo.CreatedBy,cmi.AutoScheduleServiceTypeID,cmi.EVVPatientId
FROM dbo.CallMaintenance cmi WITH (NOLOCK)
INNER JOIN dbo.CallMaintenance cmo WITH (NOLOCK) ON cmi.VendorID = cmo.VendorID
	AND cmi.CallType = 'I'
	AND cmo.CallType = 'O'
	and cmi.AideID = 2051407
	and cmo.AideID = 2051407
	AND cmi.AssignmentID = 104170
	AND cmo.AssignmentID = 104170
	AND cast(cmi.CallTime AS date) = '05-20-2020'
	AND cast(cmo.CallTime AS date) = '05-20-2020'


	SELECT cmi.CallType,cmo.CallType, cmi.CallTime,cmo.CreatedBy,cmi.AutoScheduleServiceTypeID,cmi.AssignmentID,cmi.EVVPatientId
FROM dbo.CallMaintenance cmi WITH (NOLOCK)
INNER JOIN dbo.CallMaintenance cmo WITH (NOLOCK) ON cmi.VendorID = cmo.VendorID
	AND cmi.CallType = 'I'
	AND cmo.CallType = 'O'
	AND cmi.VendorID = 964
	
	--and cmi.AideID = 2051407
	--and cmo.AideID = 2051407
	--AND cmi.AssignmentID = 104170
	--AND cmo.AssignmentID = 104170
	AND cast(cmi.CallTime AS date) = '05-19-2020'
	AND cast(cmo.CallTime AS date) = '05-19-2020'

	select * FROM LinkedCalls with(NOLOCK) where EVVPatientId = 10129855 and CAST(CallTime as date) <= '05-18-2020' AND CreatedBy =-9 AND StatusID = 22
	SELECT * FROM CallMaintenance with(NOLOCK) where EVVPatientId = 10129855 and CAST(CallTime as date) = '05-20-2020'

	SELECT VendorID FROM VendorPatients with(NOLOCK) where PatientID = 10129855
	SELECT * from TblVisits with(NOLOCK) where PatientID = 10129855 and CAST(VisitDate AS date) = '2020-05-20'

	SELECT TOP 10000 * from TblVisits with(NOLOCK) where VendorID = 964 AND Createdby = -9 AND vContractID = 25588 order BY 1 desc
	SELECT TOP 10000 * from TblVisits with(NOLOCK) where VendorID = 964 AND Createdby = -9 AND vContractID = 25588 AND PatientID = 10129855 order BY 1 desc
	--SELECT * from Admin.Search('automatic sche')

	SELECT * FROM dbo.Contracts with(NOLOCK) where ContractName = 'Medicaid' AND VendorID = 964
	SELECT ProviderVersion,AppVersionID, * FROM Vendors where VendorID = 236

--	SELECT * FROM vendors where vendorname = 'Committed Home Care'

	select * FROM VendorPatients with(NOLOCK) WHERE PatientID = 10129855

	SELECT TOP 1
				pm.PatientID,
				vp.OfficeID,
				pm.PatientHomePhone,
				pm.PatientHomePhone2,
				pm.PatientHomePhone3,				
				a.ServiceCodeID,
				pc.ContractID
	FROM TblPatientMaster pm (NOLOCK)
			INNER JOIN dbo.VendorPatients vp (NOLOCK) ON vp.PatientID = pm.PatientID 
				AND vp.VendorID = 964
				--AND pm.PatientID = 10129855
				AND '3144613011' IN (pm.PatientHomePhone,pm.PatientHomePhone2, pm.PatientHomePhone3)
				AND pm.ChhaID IS NULL		-- Internal Ptients only
				AND vp.VendorPatientStatus = 3 -- active patient
				AND ISNULL(vp.DELETED,0) = 0 -- not deleted			
				AND ISNULL(vp.DisableAutoCreateVisitBasedOnEVV, 'False') <> 'True'
			INNER JOIN dbo.PatientContracts pc (NOLOCK) ON pc.PatientID = pm.PatientID
				AND pc.IsPrimaryContract = 1 -- added by Shivam Parikh - 08/19/2015
			INNER JOIN dbo.ChhaVendors cv WITH (NOLOCK) ON pc.ContractChhaID = cv.ChhaID
				AND cv.VendorID = vp.VendorID
				AND cv.AutoCreateVisitBasedOnEVVConfirmation = 1 -- Auto Create visit based on Evv confirmation must be enabled per contracttion
			INNER JOIN [dbo].[Admissions] a (NOLOCK) ON a.PatientID = pm.PatientID
				AND a.ContractID = pc.ContractID -- Resolved by Shivam Parikh - 08/18/2015
				AND '05-20-2020' BETWEEN a.StartDate AND ISNULL(a.DischargeDate, '2075-12-31')
				ORDER BY a.StartDate DESC

				SELECT pm.*
				FROM TblPatientMaster pm (NOLOCK)
			INNER JOIN dbo.VendorPatients vp (NOLOCK) ON vp.PatientID = pm.PatientID 
				AND vp.VendorID = 236
				AND pm.PatientID = 10129855
				--AND '3144613011' IN (pm.PatientHomePhone,pm.PatientHomePhone2, pm.PatientHomePhone3)
				AND pm.ChhaID IS NULL		-- Internal Ptients only
				--AND vp.VendorPatientStatus = 3 -- active patient
				--AND ISNULL(vp.DELETED,0) = 0 -- not deleted			