USE HHA
GO

DECLARE @cmd VARCHAR(MAX), @date DATETIME

SET @date = getdate()

SET @cmd = 'SELECT ubv.VisitUnbilledDetailID INTO deleted.dbo.DeleteUnbilledDetailsNewPreBilling_'+FORMAT(@date,'MMddyyyyhhmm')+'
FROM visit.UnbilledDetails ubv (nolock)
INNER JOIN TblVisits V (NOLOCK) ON V.VisitID = ubv.VisitID
INNER JOIN Vendors VS (NOLOCK) ON V.VendorID = VS.VendorID
	AND VS.IsDemo = 0
	AND VS.Status = 1 
	AND V.Billed = 1
	AND V.Deleted = 0
	AND V.PermanentDelete = 0
	AND vS.NewPrebillingChanges = ''Yes''
	AND DATEDIFF(MINUTE,V.LastModifiedDate, GETDATE()) > 180 
'
EXEC (@cmd)

SET @cmd = '
EXEC SetContextInfoUserID @UserID = -100

BEGIN TRAN

	DELETE ubv
	FROM visit.UnbilledDetails ubv 
	WHERE ubv.VisitUnbilledDetailID IN (SELECT VisitUnbilledDetailID from deleted.dbo.DeleteUnbilledDetailsNewPreBilling_'+FORMAT(@date,'MMddyyyyhhmm')+')

COMMIT TRAN'

EXEC(@cmd)