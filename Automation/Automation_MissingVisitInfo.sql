USE HHA
GO

DECLARE @cmd VARCHAR(MAX), @date DATETIME

SET @date = getdate()

SET @cmd = 'SELECT
		            ISNULL (VisitID,'' '') AS VisitId 
		            ,ISNULL (VisitDate,'' '') as VisitDate
		            ,ISNULL (VendorName,'' '') as VendorName
            INTO deleted.dbo.VisitInfoMissing_'+FORMAT(@date,'MMddyyyyhhmm')+'
            FROM (
            SELECT 
	            v.VisitID,
	            CONVERT(VARCHAR(20),v.VisitDate,22) AS VisitDate,
	            vd.VendorName 
            FROM dbo.tblVisits V WITH(NOLOCK)
            INNER JOIN dbo.Vendors VD WITH(NOLOCK) ON V.VendorID = VD.VendorID
	            AND v.createddate <= DATEADD(minute, -120, GETDATE())
	            AND VD.IsDemo = 0
	            AND V.Deleted = 0
            LEFT JOIN VisitInfo vi (NOLOCK) ON vi.visitid = v.visitID
            WHERE vi.visitID IS NULL    
            ) A
            ORDER BY a.VendorName ASC, CAST(a.VisitDate AS DATETIME) ASC
'
EXEC (@cmd)

SET @cmd = '
	EXEC SetContextInfoUserID @UserID = -100

	BEGIN TRAN

		DELETE V
		FROM TblVisits V WHERE V.Visitid IN (SELECT VisitId from deleted.dbo.VisitInfoMissing_'+FORMAT(@date,'MMddyyyyhhmm')+')

	COMMIT TRAN'

EXEC(@cmd)