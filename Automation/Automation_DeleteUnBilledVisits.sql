USE HHA
GO

DECLARE @cmd VARCHAR(MAX), @date DATETIME

SET @date = GETDATE()

SET @cmd = 'SELECT ubv.UnBilledVisitID INTO deleted.dbo.DeleteUnbilledDetails_'+FORMAT(@date,'MMddyyyyhhmm')+'
FROM dbo.UnBilledVisits ubv (nolock)
INNER JOIN dbo.TblVisits V (NOLOCK) ON V.VisitID = ubv.VisitID
INNER JOIN dbo.Vendors VS (NOLOCK) ON V.VendorID = VS.VendorID
	AND VS.IsDemo = 0
	AND VS.Status = 1 
	AND V.Billed = 1
	AND V.Deleted = 0
	AND V.PermanentDelete = 0
	AND ISNULL(vS.NewPrebillingChanges,''No'') = ''No''
	AND DATEDIFF(MINUTE,V.LastModifiedDate, GETDATE()) > 180 
'
EXEC (@cmd)

SET @cmd = '
EXEC SetContextInfoUserID @UserID = -100

BEGIN TRAN

	DELETE ubv
	FROM dbo.UnBilledVisits ubv 
	WHERE ubv.UnBilledVisitID IN (SELECT UnBilledVisitID from deleted.dbo.DeleteUnbilledDetails_'+FORMAT(@date,'MMddyyyyhhmm')+')

COMMIT TRAN'

EXEC (@cmd)