USE HHA
GO

DECLARE @cmd VARCHAR(MAX), @date DATETIME

SET @date = getdate()

SET @cmd = 'SELECT  pa.PatientAddressID, pm.PatientID,pm.Address1,pm.Address2,pm.State,pm.CrossStreet,pm.UpdatedDate,pm.UpdatedBy,
					pm.PayerPatientID, (pa.Address1) AS PatientAddress1 ,(pa.Address2) AS PatientAddress2,(pa.State) AS PatientState,
					(pa.CrossStreet) AS PatientCrossStreet,PA.City,PA.ZipCode
			INTO deleted.dbo.PayerPatientMisMatch_'+FORMAT(@date,'MMddyyyyhhmm')+'
			FROM Payer.Patients pm (NOLOCK) 
			INNER JOIN TblCHHAMaster CM WITH(NOLOCK) ON CM.CHHAID = PM.PayerID
				AND CM.IsDemo = 0
				AND pm.Status	= ''Active''
			INNER JOIN Patient.Addresses(NOLOCK) pa ON pa.PatientID = pm.PatientID
				AND pa.PrimaryAddress	 = ''Yes''
				AND (isnull(pa.Address1,'''') <> isnull(PM.Address1,'''') OR isnull(pa.Address2,'''') <> isnull(pm.Address2,'''') OR isnull(pa.State,'''') <> isnull(PM.State,''''))
'
EXEC (@cmd)

SET @cmd = '
	EXEC SetContextInfoUserID @UserID = -100

    BEGIN TRAN

		UPDATE pm
		SET
			pm.Address1		= pa.Address1,
			pm.Address2		= pa.Address2,
			pm.State		= pa.[State],
			pm.CrossStreet	= pa.CrossStreet,
			pm.UpdatedBy	= -100,
			pm.UpdatedDate	= GETDATE()
		FROM Payer.Patients pm (NOLOCK) 
		INNER JOIN TblCHHAMaster CM WITH(NOLOCK) ON CM.CHHAID = PM.PayerID
			AND CM.IsDemo = 0
			AND pm.Status	= ''Active''
		INNER JOIN Patient.Addresses(NOLOCK) pa ON pa.PatientID = pm.PatientID
			AND pa.PrimaryAddress	 = ''Yes''
			AND (isnull(pa.Address1,'''') <> isnull(PM.Address1,'''') OR isnull(pa.Address2,'''') <> isnull(pm.Address2,'''') OR isnull(pa.State,'''') <> isnull(PM.State,''''))
                

        UPDATE PM
		SET PM.ZipCode = T.ZipCode,
			PM.City = T.City,
			PM.UpdatedBy = -100,
			PM.UpdatedDate = GETDATE(),
			PM.UpdatedUTCDate = GETUTCDATE()
		FROM [PayerPHI].[PHI].[Patients] PM
		INNER JOIN deleted.dbo.PayerPatientMisMatch_'+FORMAT(@date,'MMddyyyyhhmm')+' T ON T.PatientID = PM.PatientID
				 

    COMMIT TRAN'

EXEC(@cmd)