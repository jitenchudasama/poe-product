USE HHA
GO

DECLARE @cmd VARCHAR(MAX), @date DATETIME

SET @date = getdate()

SET @cmd = 'WITH a as(
                SELECT MAX(CaregiverComplianceCustomFieldID) as id, CaregiverID , ComplianceFieldID ,CustomFieldName,CustomFieldType,CustomFieldLabel, count(1) as co1
                FROM Caregiver.ComplianceCustomFields (NOLOCK)
                GROUP by CaregiverID , ComplianceFieldID,CustomFieldName,CustomFieldType,CustomFieldLabel
                HAVING count(1) > 1)

                SELECT c.* 
                INTO deleted.dbo.DuplicateCompliance_'+FORMAT(@date,'MMddyyyyhhmm')+'
                FROM Caregiver.ComplianceCustomFields c (NOLOCK)
                INNER join a on c.CaregiverID = a.CaregiverID
                WHERE c.CustomFieldName = a.CustomFieldName AND c.CustomFieldType = a.CustomFieldType AND c.CustomFieldLabel = a.CustomFieldLabel 
                ORDER By a.CaregiverID ASC, a.CustomFieldLabel ASC
'
EXEC (@cmd)

SET @cmd = '
EXEC SetContextInfoUserID -100

BEGIN TRY
        DECLARE @cnt INT = 1

        WHILE @cnt > 0 BEGIN

            BEGIN TRAN

                DELETE TOP(100) c 
				FROM Caregiver.ComplianceCustomFields c 
                INNER JOIN deleted.dbo.DuplicateCompliance_'+FORMAT(@date,'MMddyyyyhhmm')+' a on c.CaregiverComplianceCustomFieldID = a.CaregiverComplianceCustomFieldID

                IF @@Rowcount <=0 BEGIN
                    SET @cnt = 0
                END    
            COMMIT TRAN
        END    
END TRY
BEGIN CATCH
    IF @@ROWCOUNT > 0
    BEGIN
        ROLLBACK TRAN
    END
END CATCH'

EXEC(@cmd)