USE HHA
GO

DECLARE @cmd VARCHAR(MAX), @date DATETIME

SET @date = getdate()

SET @cmd = 'SELECT DISTINCT pm.PatientID,pm.StatusID 
            INTO deleted.dbo.PayerPatientActivePatient_'+FORMAT(@date,'MMddyyyyhhmm')+'
            FROM TblPatientMaster pm WITH(NOLOCK)
            INNER JOIN VendorPatients VP WITH(NOLOCK) ON VP.PatientID = PM.PatientID
                AND VP.VendorPatientStatus = 3
                AND PM.StatusID NOT IN (3,6)
            INNER JOIN Vendors(NOLOCK) v ON v.VendorID = vp.VendorID
                AND v.IsDemo = 0
                AND v.Status = 1
            INNER JOIN CurrentPlacements CP WITH(NOLOCK) ON CP.PatientID = VP.PatientID
                AND CP.VendorID = VP.VendorID
                AND CP.StatusID = 3
            INNER JOIN Payer.Patients PP WITH(NOLOCK) ON PP.PatientID = PM.PatientID
                AND PP.Status = ''Active''
'
EXEC (@cmd)

SET @cmd = '
	EXEC SetContextInfoUserID @UserID = -100

    BEGIN TRAN

    UPDATE pm
	    SET StatusID = 3,
		    LastModifiedBy = -100,
		    LastModifiedDate = GETDATE()
	    --SELECT pm.patientid,pm.statusid
    FROM TblPatientMaster pm 
	INNER JOIN deleted.dbo.PayerPatientActivePatient_'+FORMAT(@date,'MMddyyyyhhmm')+' T ON T.PatientID = pm.PatientID

    COMMIT TRAN'

EXEC(@cmd)