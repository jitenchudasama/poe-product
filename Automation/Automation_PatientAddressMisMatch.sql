USE HHA
GO

DECLARE @cmd VARCHAR(MAX), @date DATETIME

SET @date = getdate()

SET @cmd = 'SELECT  pm.PatientID,
					pa.PrimaryAddress, 
					pa.AddressTypes, 
					pa.Address1 AS PatientAddress1, 
					pm.Address1, 
					pm.city, 
					pm.State, 
					pm.CreatedBy, 
					pm.CreatedDate, 
					pm.LastModifiedBy, 
					pm.LastModifiedDate, 
					vp.VendorID, 
					v.VendorName, 
					cm.ChhaName
            INTO deleted.dbo.PatientMisMatch_'+FORMAT(@date,'MMddyyyyhhmm')+'
            FROM TblPatientMaster(NOLOCK) pm
            INNER JOIN VendorPatients (NOLOCK) VP ON pm.PatientID = vp.PatientID
            INNER JOIN Vendors(NOLOCK) V ON v.VendorID = vp.VendorID
                AND v.IsDemo = 0
            LEFT JOIN tblChhaMaster(NOLOCK) cm ON cm.ChhaID = pm.ChhaID
            INNER JOIN Patient.Addresses(NOLOCK) pa ON pa.PatientID = pm.PatientID
                AND ISNULL(pa.Address1, '''') <> ISNULL(pm.Address1, '''')
            WHERE pm.Address1 IS NULL
                AND ISNULL(pa.Address1, '''') <> ''''
                AND ISNULL(cm.IsDemo, 0) = 0
            ORDER BY pm.PatientID DESC
'
EXEC (@cmd)

SET @cmd = '
	EXEC SetContextInfoUserID @UserID = -100

    BEGIN TRAN

        UPDATE pm
        SET
	        pm.Address1			= pa.Address1,
	        pm.Address2			= pa.Address2,
	        pm.State			= pa.[State],
	        pm.City				= pa.City,
	        pm.ZipCode			= pa.ZipCode,
	        pm.CrossStreet		= pa.CrossStreet,
	        pm.LastModifiedBy	= um.ID,
	        pm.LastModifiedDate = GETDATE()
        FROM TblPatientMaster pm 
		INNER JOIN deleted.dbo.PatientMisMatch_'+FORMAT(@date,'MMddyyyyhhmm')+' T ON T.PatientID = pm.patientid
        INNER JOIN patient.addresses pa with(nolock) on  pa.patientid = pm.patientid
			AND (pa.addresstypes = ''Billing, GPS'' or pa.addresstypes = ''GPS, Billing'')
        INNER JOIN tblUserMaster um with(nolock) on um.id =  pm.ChhaId
			AND um.IsSupportUser =  1 AND um.UserName like ''%Payer Data%''

     COMMIT TRAN'

EXEC(@cmd)